## qssi-user 13 TP1A.220905.001 1675265110324 release-keys
- Manufacturer: oplus
- Platform: kalama
- Codename: ossi
- Brand: oplus
- Flavor: qssi-user
- Release Version: 13
- Kernel Version: 5.15.41
- Id: TP1A.220905.001
- Incremental: 1675265110324
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: oplus/ossi/ossi:13/TP1A.220905.001/1675170186363:user/release-keys
- OTA version: 
- Branch: qssi-user-13-TP1A.220905.001-1675265110324-release-keys
- Repo: oplus_ossi_dump_16132
