{
    "Image_Build_IDs": {
        "adsp": "ADSP.HT.5.8-00957.1-KAILUA-1.17968.6",
        "aop": "AOP.HO.5.0-00212-KAILUA_E-1.17626.8",
        "boot": "BOOT.MXF.2.1.1-00067-KAILUA-1.17626.2",
        "btfm_hmt": "BTFW.HAMILTON.2.0.0-00498-PATCHZ-1",
        "cdsp": "CDSP.HT.2.8-00609-KAILUA-1.17968.2",
        "common": "Kailua.LA.1.0.r1-00941-STD.PROD-2.543719.2",
        "cpucp": "CPUCP.FW.1.0-00058-KAILUA.EXT-1.543719.3",
        "cpuss_vm": "CPUSS.CPUSYS.VM.1.0-00006-KAILUA.EXT-1",
        "glue": "GLUE.KAILUA.LA.1.0-00328-NOOP_TEST_KAILUA-1",
        "modem": "MPSS.DE.3.0-02130.2-KAILUA_GEN_PACK-1",
        "spss": "SPSS.A1.1.7-00069-KAILUA-2",
        "tz": "TZ.XF.5.24-00065-KAILUAAAAAANAZT-1.20487.4",
        "tz_apps": "TZ.APPS.1.24-00024-KAILUAAAAAANAZT-1.20487.2",
        "wlan_hmt": "WLAN.HMT.1.0-03586-QCAHMTSWPL_V1.0_V2.0_SILICONZ-2"
    },
    "Metabuild_Info": {
        "Meta_Build_ID": "Kailua.LA.1.0.r1-00941-STD.PROD-2.543719.2",
        "Product_Flavor": "asic",
        "Time_Stamp": "2023-01-31 21:02:57"
    },
    "Version": "1.0"
}